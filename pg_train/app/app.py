""" Probability graph module """
import json
import logging
import math
import os
import operator
import pickle
from collections import OrderedDict
import numpy as np
from xpresso.ai.core.logging.xpr_log import XprLogger


# Model saving function
def save_model(data, filename):
    """ Saving graph """
    output_dict = open(filename, 'wb')
    pickle.dump(data, output_dict)
    output_dict.close()
    return


# Merging two dict
def merge_dict(olddata, newdata):
    """ Merging splitted dict """
    for key, val in newdata.items():
        if key in olddata:
            val[0] += olddata[key][0]
            val[1] += olddata[key][1]
        olddata[key] = val
    return olddata


# Probability graph class
class PGraphTrain(object):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.mountedpath = self.data_prep_param['mounted_path']
        self.inputfile = self.data_prep_param['input_file']
        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.folders = self.data_prep_param['folders']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.trainfileseq = self.datapath + self.data_prep_param['train_file_seq']

        self.lookahead = self.data_model['lookahead_window']
        self.modelpath = self.mountedpath + self.data_model['model_path_pg']
        self.logpath = self.modelpath + self.data_prep_param['logfile_train']

        self.pg_split = self.modelpath + self.data_model['output_files']['pg_split']
        self.temp_split = self.modelpath + self.data_model['output_files']['train_split_seq']

        self.block_splitter = self.data_model['block_splitter_length']
        self.seq_splitter = self.data_model['seq_splitter_length']

        self.window = [-1] * self.lookahead
        self.window_size = 0
        self.lines = []
        self.edges = OrderedDict()
        self.weight = [round(math.exp(item * -0.65), 5) for item in range(self.lookahead - 1, 0, -1)]
        self.all_blocks = []
        self.block_perc_norm = []
        self.seq_perc_norm = []
        self.master_dict = {}

        self.my_logger = logging.getLogger()

    def load_train_file(self):
        """ Reading file and dividing block access intoF sequences """
        with open(self.trainfileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = line_num
        with open(self.trainfileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info("loading %s ", str(loading_val))
                    print("loading train data", str(loading_val))
                train_seq = line.replace("\n", "").split(" ")
                self.lines.append(train_seq)

    def add_to_edges(self, src, dst, factor):
        """ Adding or updating edges """
        if src in self.edges:
            if dst in self.edges[src]:
                self.edges[src][dst][0] += 1
                self.edges[src][dst][1] += factor
                return
            self.edges[src][dst] = [1, factor]
        else:
            self.edges[src] = {dst: [1, factor]}

    def add_to_window(self, block_id):
        """ Adding node to lookahead window """
        if self.window_size < self.lookahead:
            self.window[self.window_size] = block_id
            self.window_size += 1
        else:
            self.window = self.window[1:] + [block_id]

    def train_helper(self):
        """ Getting sequence spliiter """
        for ioaccess in self.lines:
            self.all_blocks += ioaccess
        self.all_blocks = sorted(list(set(self.all_blocks)))

        temp_val = 0.00002 * len(self.lines) + 0.00018 * len(self.all_blocks) + 56
        estimated_time = np.ceil(temp_val * 1.1 / 60.0)
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)

        block_perc = range(len(self.all_blocks))
        min_val = min(block_perc) - 1
        block_len = float(max(block_perc) - min_val) / self.block_splitter
        self.block_perc_norm = \
            [int(np.ceil(float(item - min_val) / block_len)) - 1 for item in block_perc]

        seq_perc = range(len(self.lines))
        min_val = min(seq_perc) - 1
        seq_len = float(max(seq_perc) - min_val) / self.seq_splitter
        self.seq_perc_norm = [int(np.ceil(float(item - min_val) / seq_len)) - 1 for item in seq_perc]

        _ = [os.remove(self.modelpath + item) for item in os.listdir(self.modelpath) \
             if item.endswith('.pkl')]

    def split_blocks(self, filename):
        """ Splitting based on blockids"""
        for dict_num in range(self.block_splitter):
            temp_dict = {}
            for ind, val in enumerate(self.block_perc_norm):
                if val == dict_num:
                    key = self.all_blocks[ind]
                    if key not in self.edges:
                        continue
                    else:
                        temp_dict[key] = self.edges[key]
                        self.edges.pop(key)
                else:
                    continue
            temp_filename = filename.replace("BLOCK", str(dict_num))
            save_model(temp_dict, temp_filename)

    def normalize_dict(self):
        """ Normalize edge weight """
        for block_id, val in self.master_dict.items():
            out = sum([item[0] for key, item in val.items()])
            for key, item in val.items():
                self.master_dict[block_id][key] = round(item[1] / float(out), 5)

    def sort_dict(self):
        """ Sort edge by weight """
        for block_id in self.master_dict.keys():
            self.master_dict[block_id] = \
                sorted(self.master_dict[block_id].items(), key=operator.itemgetter(1), reverse=True)

    def clubbing_models(self):
        """ Merging models generated on sharded files """
        for block_num in range(self.block_splitter):
            for ind, seq_num in enumerate(range(self.seq_splitter)):
                filenum = self.temp_split
                filenum = filenum.replace('SEQ', str(seq_num)).replace('BLOCK', str(block_num))
                self.my_logger.info("%s", filenum)
                print("Current file clubbing", filenum)
                if not ind:
                    self.master_dict = pickle.load(open(filenum, 'rb'))
                else:
                    slave_dict = pickle.load(open(filenum, 'rb'))
                    for key, val in slave_dict.items():
                        if key in self.master_dict:
                            val = merge_dict(self.master_dict[key], val)
                        self.master_dict[key] = val
                os.remove(filenum)
            self.normalize_dict()
            self.sort_dict()
            filename = self.pg_split
            filename = filename.replace('VAL', str(block_num + 1))
            save_model(self.master_dict, filename)

        del self.master_dict

    def train(self):
        """ Training module """
        self.train_helper()
        current_seq_num = self.seq_perc_norm[0]
        for kid, ioaccess in enumerate(self.lines):
            if self.seq_perc_norm[kid] != current_seq_num:
                temp_filename = self.temp_split.replace("SEQ", str(current_seq_num))
                self.split_blocks(temp_filename)
                current_seq_num = self.seq_perc_norm[kid]

            self.window_size = 0
            for block_id in ioaccess:
                self.add_to_window(block_id)
                for winj in range(self.window_size - 1):
                    weight_index = winj + self.lookahead - self.window_size
                    self.add_to_edges(self.window[winj], block_id, self.weight[weight_index])
            if not (kid + 1) % 100000:
                training_val = round(float(kid) * 100.0 / float(len(self.lines)), 2)
                self.my_logger.info("training %s ", str(training_val))
                print("training ", str(training_val))

        if self.edges:
            temp_filename = self.temp_split.replace("SEQ", str(current_seq_num))
            self.split_blocks(temp_filename)

        del self.lines, self.seq_perc_norm, self.block_perc_norm, self.all_blocks

        self.clubbing_models()

    def create_logger(self):
        """ Creating logger """
        path = os.getcwd()
        config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
        self.my_logger = XprLogger(config_path=config_file)

    def main(self):
        """ Main method"""

        # Creating logger
        self.create_logger()

        self.my_logger.info("Process started")
        print("Process started")

        # Loading and parsing train file
        self.my_logger.info("Loading and parsing train file")
        print("Loading and parsing train file")
        self.load_train_file()

        # Building probability graph model
        self.my_logger.info("Building probability graph model")
        print("Building probability graph model")
        self.train()


# Creating class object
CLASS_OBJ = PGraphTrain()

# Calling main method of class
CLASS_OBJ.main()
