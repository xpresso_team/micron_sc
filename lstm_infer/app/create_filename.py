def create_filename(config):
    return ("_epochs_" + str(config['lstm']['epochs']) + "_batch_size_" + str(
        config['lstm']['batch_size']) + "_lstm_" + str(config['lstm']['lstm_no']) + "_dropout_" + str(
        config['lstm']['dropout']) + "_optimizer_" + str(config['lstm']['optimizer']))
