from __future__ import print_function
import numpy as np
import os
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from app.create_filename import create_filename
import json
from flask import Flask, request, jsonify, Response
import pickle
from xpresso.ai.core.logging.xpr_log import XprLogger

cur_work_dir = os.getcwd()
app = Flask(__name__)

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

use_gpu_bool = config['lstm']['use_gpu']
num_of_gpu = len(config['lstm']['cuda_visible_devices'].split(","))

if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    if num_of_gpu == 1:
        config['lstm']['use_gpu'] = False
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)

''' Creating logger '''
path = os.getcwd()
config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
my_logger = XprLogger(config_path=config_file)


@app.route("/lstm_inference", methods=["POST"])
def inference():
    request_body = request.get_json()
    response = dict()
    my_logger.info("Request Body : " + str(request_body) + "\n")
    if 'InputSequence' not in request.get_json() or 'Topk' not in request.get_json():
        return Response(
            status=404,
            response="Required Parameters not found"
        )
    try:
        input_sequence = request_body['InputSequence']
        k = int(request_body['Topk'])
        input_sequence = input_sequence.split()
        input_sequence_tokenized = tokenizer.texts_to_sequences([input_sequence])[0]
        input_sequence_padded = pad_sequences([input_sequence_tokenized], maxlen=max_length - 1, padding='pre')

        predicted_ids = np.ndarray.tolist(np.argsort(model.predict(input_sequence_padded), axis=1))[0]
        predicted_ids.reverse()
        resp_seq = list()

        for index, value in enumerate(predicted_ids):
            if index >= k:
                break
            resp_seq.append(index_to_id[value])
        return Response(
            status=200,
            response=json.dumps({"predicted sequences": resp_seq})
        )

    except Exception as e:
        return Response(status=200, response=str(e))


def main():
    app.run(host='0.0.0.0', port=8000, threaded=False, debug=False, use_reloader=False)


if __name__ == "__main__":

    my_logger.info("Loading Tokenizer...")
    # Loading tokenizer
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    index_to_id = {v: k for k, v in tokenizer.word_index.items()}
    my_logger.info("Tokenizer Loading Completed")

    # Loading max length
    my_logger.info("Loading Max Sequence Length...")
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
        max_length = pickle.load(handle)
    handle.close()
    my_logger.info("Max Sequence Length Loading Completed")

    # Loading model
    my_logger.info("Loading model ...")
    model_name = "model" + create_filename(config) + ".h5"
    if os.path.exists(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   config['lstm']['model_save_path'], model_name))):
        model = load_model(os.path.abspath(
            os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                         str(config['lstm']['model_save_path']), str(model_name))))

        use_gpu_bool = config["lstm"]["use_gpu"]
    my_logger.info("Model loading completed")
    main()
