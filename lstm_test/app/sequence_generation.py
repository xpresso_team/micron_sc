from __future__ import print_function
from datareading import load_data
import os
import pickle
from itertools import islice
from create_filename import create_filename
import json
from sklearn.model_selection import train_test_split

cur_work_dir = os.getcwd()


def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result


def sequence_generation(config, master_path, type, my_logger):
    sequences = list()
    if type == "_train_":
        data = load_data(master_path, config, True, False, my_logger)
    elif type == "_test_":
        data = load_data(master_path, config, False, True, my_logger)
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    handle.close()

    slidingwindow = config["lstm"]["slidingwindow"]
    slidingwindowbool = config["lstm"]["sliding_window_bool"]

    for line in data:
        encoded = tokenizer.texts_to_sequences([line])[0]
        if len(encoded) > slidingwindow and slidingwindowbool:
            for enc in window(encoded, slidingwindow):
                for i in range(1, len(enc)):
                    sequence = enc[:i + 1]
                    sequences.append(sequence)
        elif len(encoded) <= slidingwindow and len(encoded) > 1:
            for i in range(1, len(encoded)):
                sequence = encoded[:i + 1]
                sequences.append(sequence)

    sequence_filename = "sequence" + type + create_filename(config) + ".pickle"

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], sequence_filename)), 'wb') as handle:
        pickle.dump(sequences, handle, protocol=pickle.HIGHEST_PROTOCOL)
    handle.close()

    my_logger.info('Total Sequences: %d' % len(sequences))

    return sequences


if __name__ == '__main__':
    config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
    sequence_generation(config, "_train_")
