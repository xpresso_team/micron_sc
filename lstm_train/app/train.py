import os
import json
from xpresso.ai.core.logging.xpr_log import XprLogger

cur_work_dir = os.getcwd()
config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)

use_gpu_bool = config['lstm']['use_gpu']
num_of_gpu = len(config['lstm']['cuda_visible_devices'].split(","))

if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    if num_of_gpu == 1:
        config['lstm']['use_gpu'] = False
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

from app.datareading import datareading
from app.sequence_generation import sequence_generation
from app.model_train import train_test

if __name__ == '__main__':
    ''' Creating logger '''
    path = os.getcwd()
    config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
    my_logger = XprLogger(config_path=config_file)

    datareading(master_path, config, my_logger)
    sequence_generation(config, master_path, "_train_", my_logger)
    train_test(config, master_path, "_train_", my_logger)
