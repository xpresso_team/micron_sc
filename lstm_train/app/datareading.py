from __future__ import print_function
from keras.preprocessing.text import Tokenizer
import pickle
import os
from app.create_filename import create_filename
import os
import json

cur_work_dir = os.getcwd()


def load_data(master_path, config, train_bool, test_bool, my_logger):
    data = []
    try:
        if train_bool:
            with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   './data/train_seq.txt'))) as f:
                for line in f:
                    data.append(line)
            f.close()
        if test_bool:
            with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   './data/validate_seq.txt'))) as f:
                for line in f:
                    data.append(line)
            f.close()
    except NameError:
        my_logger.info("Required field not defined in config")
        return None
    return data


def datareading(master_path, config, my_logger):
    data = load_data(master_path, config, True, True, my_logger)
    if data is None:
        return

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(data)
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    vocab_size = len(tokenizer.word_index)
    my_logger.info("voacb size: " + str(vocab_size))
    return vocab_size


if __name__ == '__main__':
    config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
    datareading(config)
