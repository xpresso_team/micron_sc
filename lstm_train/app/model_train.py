from __future__ import print_function
from numpy import array
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, Conv1D, MaxPooling1D, Dropout, Activation
from keras.layers import Embedding
import tensorflow as tf
import os
from keras.utils import multi_gpu_model
import pickle
from tensorflow.python.client import device_lib
from keras.models import load_model
from app.create_filename import create_filename
from sklearn.model_selection import train_test_split
import json

config = tf.ConfigProto()
config.intra_op_parallelism_threads = 500
config.inter_op_parallelism_threads = 500

cur_work_dir = os.getcwd()


def padding(master_path, config, type, my_logger):
    sequence_filename = "sequence" + type + create_filename(config) + ".pickle"
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], sequence_filename)), 'rb') as handle:
        sequences = pickle.load(handle)
    handle.close()

    if type == "_train_":
        max_length = max([len(seq) for seq in sequences])
        with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                               config['lstm']['pickle_path'], max_length_filename)), 'wb') as handle:
            pickle.dump(max_length, handle, protocol=pickle.HIGHEST_PROTOCOL)
        handle.close()

    elif type == "_test_":
        with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                               config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
            max_length = pickle.load(handle)
        handle.close()

    my_logger.info("Max length sequence : %d" % max_length)

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    vocab_size = len(tokenizer.word_index)
    my_logger.info("voacb size: " + str(vocab_size))

    sequences = pad_sequences(sequences, maxlen=max_length, padding='pre')
    sequences = array(sequences)
    X, y = sequences[:, :-1], sequences[:, -1]
    return X, y, vocab_size, max_length


def train_test(config, master_path, type, my_logger):
    my_logger.info(device_lib.list_local_devices())
    if type == "_train_":
        X_train, y_train, vocab_size, max_length = padding(master_path, config, type, my_logger)

        use_gpu_bool = config["lstm"]["use_gpu"]
        load_weight_bool = config['lstm']['load_weight_bool']

        my_logger.info('Build model...')
        model = Sequential()
        model.add(Embedding(vocab_size + 1, 100, input_length=max_length - 1))
        model.add(Dropout(config['lstm']['dropout']))
        model.add(LSTM(config['lstm']['lstm_no']))
        model.add(Dense(vocab_size + 1, activation='softmax'))
        my_logger.info(model.summary())

        if use_gpu_bool:
            model = multi_gpu_model(model)

        if load_weight_bool:
            model.load_weights(os.path.abspath(
                os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                             config['lstm']['model_save_path'], config['lstm']['load_weight_model_name'])))

        # try using different optimizers and different optimizer configs
        '''
        model.compile(loss='sparse_categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])
        '''
        my_logger.info('Train...')

        model.compile(loss='sparse_categorical_crossentropy', optimizer=config['lstm']['optimizer'],
                      metrics=["accuracy"])
        history_callback = model.fit(X_train, y_train, validation_split=config['lstm']['validation_split'],
                                     batch_size=config['lstm']['batch_size'], epochs=config['lstm']['epochs'])
        history_callback = history_callback.history
        accuracy = history_callback['acc']
        loss = history_callback['loss']
        val_accuracy = history_callback['val_acc']
        val_loss = history_callback['val_loss']
        my_logger.info("Training Accuray for each epoch : \n" + str(accuracy))
        my_logger.info("Training Loss for each epoch : \n" + str(loss))
        my_logger.info("Validation Accuray for each epoch : \n" + str(val_accuracy))
        my_logger.info("Validation Loss for each epoch : \n" + str(val_loss))

        model_name = "model" + create_filename(config) + ".h5"
        my_logger.info("Saving model...")
        model.save(os.path.abspath(
            os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                         str(config['lstm']['model_save_path']), str(model_name))))

    elif type == "_test_":
        X_test, y_test, vocab_size, max_length = padding(master_path, config, type, my_logger)
        model_name = "model" + create_filename(config) + ".h5"
        if os.path.exists(os.path.abspath(
                os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                             config['lstm']['model_save_path'], model_name))):
            model = load_model(os.path.abspath(
                os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                             str(config['lstm']['model_save_path']), str(model_name))))

            model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=["accuracy"])
            score = model.evaluate(X_test, y_test)
            my_logger.info("Test score and accuracy : " + str(score))
        else:
            my_logger.info("No model found at specified directory")
    return


if __name__ == '__main__':
    config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
    use_gpu_bool = config['lstm']['use_gpu']
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    if use_gpu_bool:
        os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = " "

    train_test(config, "_train_")
