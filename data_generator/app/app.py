""" Data generation module """
import json
import logging
import os
import pickle
from random import seed
import numpy as np
from xpresso.ai.core.logging.xpr_log import XprLogger


# Folder creating function
def create_folders(dirnames, pathname):
    """ Creating folder if not present """
    for dirname in dirnames:
        folder_name = pathname + dirname
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)


# Model saving function
def save_model(data, filename):
    """ Saving graph """
    output_dict = open(filename, 'wb')
    pickle.dump(data, output_dict)
    output_dict.close()
    return


# Sequence saving function
def save_seq(filename, data):
    """ Saving sequence """
    buffer_writer = open(filename, "w")
    for line in data:
        line_temp = [str(item) for item in line]
        line_temp = " ".join(line_temp) + "\n"
        buffer_writer.writelines(line_temp)
    buffer_writer.close()


# Data generation class
class DGeneration(object):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.inputfile = self.data_prep_param['input_file']
        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.seed_val = self.data_prep_param['seed']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.trainfile = self.datapath + self.data_prep_param['train_file']
        self.validatefile = self.datapath + self.data_prep_param['validate_file']
        self.trainfileseq = self.datapath + self.data_prep_param['train_file_seq']
        self.validatefileseq = self.datapath + self.data_prep_param['validate_file_seq']

        self.folders = self.data_prep_param['folders']
        self.logpath = self.datapath + self.data_prep_param['logfile']

        self.snia_op = self.data_prep_param['convert_mode']['snia_op']
        self.op_val = self.data_prep_param['convert_mode']['op_val']

        self.my_logger = logging.getLogger()

    def get_sequence_wo_shuffle(self, lines, limit):
        """Formatting data then shuffling"""
        lines_str = [' '.join([str(it) for it in item]) for item in lines]
        seed(self.seed_val)
        # shuffle(lines_str)
        save_model(lines_str[:limit], self.trainfileseq.replace('.txt', '.pkl'))
        save_model(lines_str[limit:], self.validatefileseq.replace('.txt', '.pkl'))

    def prepare_sequence_and_shuffle(self):
        """ Reading file and dividing block access into sequences """
        previous, sentencesb, lines = 0, [], []
        with open(self.inputfile) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = int(float(line_num))
        estimated_time = np.ceil(total_records / (400000 * 60.0))
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)
        with open(self.inputfile) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info("loading file %s ", str(loading_val))
                    print(f"loading file {str(loading_val)} + %")
                fields = line.replace('\n', '').split(";")
                timestamp = float(fields[0])
                sec_num = int(fields[2])
                if self.read_write_flag:
                    operation = self.snia_op[fields[5].strip()]
                    disk_id = sec_num * 10 + self.op_val[operation]
                else:
                    disk_id = sec_num
                if (timestamp - previous > self.cutting_window) and previous:
                    lines.append(sentencesb)
                    sentencesb = []
                sentencesb += [disk_id]
                previous = timestamp
            if sentencesb:
                lines.append(sentencesb)

        limit = int(float(len(lines)) * self.splitperc)
        self.get_sequence_wo_shuffle(lines, limit)
        seed(self.seed_val)
        # shuffle(lines)
        save_seq(self.trainfileseq, lines[:limit])
        save_seq(self.validatefileseq, lines[limit:])

    def create_logger(self):
        """ Creating logger """
        path = os.getcwd()
        config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
        self.my_logger = XprLogger(config_path=config_file)

    def main(self):
        """ Main method"""

        # Creating folders
        create_folders(self.folders, self.mountedpath)

        # Creating logger
        self.create_logger()

        self.my_logger.info("Process started")
        print("Process started")

        # Generating sequences and divide sequences into train and test
        self.my_logger.info("Generating sequences and divide sequences into train and test")
        print("Generating sequences and divide sequences into train and test")
        self.prepare_sequence_and_shuffle()


# Creating class object
CLASS_OBJ = DGeneration()

# Calling main method of class
CLASS_OBJ.main()
